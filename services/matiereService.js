const matiereModel = require('../models/matiereModel');

const index = async (res) => {
    try {
        const data = await matiereModel.find();
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const find = async (id, res) => {
    try {
        const data = await matiereModel.findById(id);
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const store = async (body, res) => {
    try {
        const filiereSchema = new matiereModel(body);
        await filiereSchema.save();
        return filiereSchema; 
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const update = async (id, body, res) => {
    try {
        await matiereModel.findByIdAndUpdate(id, body);
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const destroy = async (id, res) => {
    try {
        await matiereModel.findByIdAndDelete(id);
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

module.exports = { index, find, store, update, destroy }