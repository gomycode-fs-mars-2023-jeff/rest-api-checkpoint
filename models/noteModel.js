const mongoose = require('mongoose');
const { Schema } = mongoose;

const noteSchema = Schema({
    note: Number,
    matricule: String,
    filiereId: {
        type: Schema.ObjectId,
        ref: "Filiere"
    },
    matiereId: {
        type: Schema.ObjectId,
        ref: "Matiere"
    }
})

const noteModel = mongoose.model('Note', noteSchema);

module.exports = noteModel;