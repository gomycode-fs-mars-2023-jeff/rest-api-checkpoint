const coeffModel = require('../models/coeffModel');

const index = async (res) => {
    try {
        const data = await coeffModel.find();
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const find = async (id, res) => {
    try {
            const data = await coeffModel.findById(id);
            return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }

}

const store = async (body, res) => {
    try {
        const coeffSchema = new coeffModel(body);
        await coeffSchema.save();
        return coeffSchema;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const update = async (id, body, res) => {
    try {
        await coeffModel.findByIdAndUpdate(id, body)
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const destroy = async (id, res) => {
    try {
        await coeffModel.findByIdAndDelete(id);
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

module.exports = { index, find, store, update, destroy }