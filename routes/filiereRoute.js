var express = require('express');
var router = express.Router();
const filiereController = require('../controllers/filiereController');

router.get ('/', filiereController.index);
router.get ('/:id', filiereController.find);
router.post ('/', filiereController.store);
router.put ('/:id', filiereController.update);
router.delete ('/:id', filiereController.destroy);

module.exports = router;