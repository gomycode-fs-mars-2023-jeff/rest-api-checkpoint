var express = require('express');
var router = express.Router();
const matiereController = require('../controllers/matiereController');

router.get ('/', matiereController.index);
router.get ('/:id', matiereController.find);
router.post ('/', matiereController.store);
router.put ('./:id', matiereController.update);
router.delete ('./:id', matiereController.destroy);

module.exports = router;