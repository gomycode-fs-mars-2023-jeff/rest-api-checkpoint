const mongoose = require('mongoose');
const { Schema } = mongoose;

const studentsSchema = Schema ({
    nom: String,
    prenom: String,
    matricule: String,
    sexe: String,
    age: Number
});

const studentsModel = mongoose.model('Student', studentsSchema);
module.exports = studentsModel;