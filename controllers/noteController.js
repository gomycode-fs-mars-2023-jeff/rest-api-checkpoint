const noteService = require('../services/noteService')

const index = async (req, res) => {
    try {
        const data = await noteService.index();
        res.status(200).json(data);
        
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const find = async (req, res) => {
    try {
        const data = await noteService.find(req.params.id);
        res.status(200).json(data);
    } catch (error) {
        
    }
}

const store = async (req, res) => {
    try {
        const data = await noteService.store(req.body);
        res.status(200).json(data);
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const update = async (req, res) => {
    try {
        const data = await noteService.update(req.params.id, req.body);
        res.status(200).json(data)
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const destroy = async (req, res) => {
    try {
        const data = await noteService.destroy(req.params.id);
        res.status(200).json(data);
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

module.exports = { index, find, store, update, destroy }