var express = require('express');
var router = express.Router();
const coeffController = require('../controllers/coeffController');

router.get ('/', coeffController.index);
router.get ('/:id', coeffController.find);
router.post ('/', coeffController.store);
router.put ('/:id', coeffController.update);
router.delete ('/:id', coeffController.destroy);

module.exports = router;