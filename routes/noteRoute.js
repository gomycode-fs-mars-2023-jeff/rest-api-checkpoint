var express = require('express');
var router = express.Router();
const noteController = require('../controllers/noteController');

router.get ('/', noteController.index);
router.get ('/:id', noteController.find);
router.post ('/', noteController.store)
router.put ('/:id', noteController.update);
router.delete ('/:id', noteController.destroy);

module.exports = router;