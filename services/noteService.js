const noteModel = require('../models/noteModel');

const index = async (res) => {
    try {
        const data = await noteModel.find();
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message})
    }
}

const find = async (id, res) => {
    try {
        const data = await noteModel.findById(id);
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const store = async (body, res) => {
    try {
        const noteSchema = await new noteModel(body);
        await noteSchema.save();
        return noteSchema;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const update = async (id, body, res) => {
    try {
        await noteModel.findByIdAndUpdate(id, body);
        return true;
    } catch (error) {
        res.status(error.status).josn({error: error.message});
    }
}

const destroy = async (id, res) => {
    try {
        await noteModel.findByIdAndDelete(id);
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

module.exports = { index, find, store, update, destroy }