const mongoose = require('mongoose');
const { Schema } = mongoose;

const coeffSchema = Schema ({
    coefficient: Number,
    filiereId: {
        type: Schema.ObjectId,
        ref: "Filiere"
    },
    matiereId: {
        type: Schema.ObjectId,
        ref: "Matiere"
    }
})

const coeffModel = mongoose.model('Coefficient', coeffSchema);

module.exports = coeffModel; 