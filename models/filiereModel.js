const mongoose = require('mongoose');
const { Schema } = mongoose;

const filiereSchema = Schema ({
    libéllé: String,
    description: String
})

const filiereModel = mongoose.model('Filière', filiereSchema);

module.exports = filiereModel;