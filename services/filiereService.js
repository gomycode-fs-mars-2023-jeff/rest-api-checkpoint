const filiereModel = require('../models/filiereModel');

const index = async (res) => {
    try {
        const data = await filiereModel.find();
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const find = async (id, res) => {
    try {
        const data = await filiereModel.findById(id)
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const store = async (body, res) => {
    try {
        const filiereSchema = new filiereModel(body);
        await filiereSchema.save();
        return filiereSchema;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const update = async (id, body, res) => {
    try {
        await filiereModel.findByIdAndUpdate(id, body);
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

const destroy = async (id, res) => {
    try {
        await filiereModel.findByIdAndDelete(id);
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
    }
}

module.exports = { index, find, store, update, destroy }