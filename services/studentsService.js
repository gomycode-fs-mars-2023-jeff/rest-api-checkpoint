const studentsModel = require('../models/studentsModel');

const index = async (res) => {

    try {
        const data = await studentsModel.find();
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
        
    }
}

const find = async (id, res) => {

    try {
        const data = await studentsModel.findById(id);
        return data;
    } catch (error) {
        res.status(error.status).json({error: error.message});
        
    }
}

const store = async (body, res) => {

    try {
        const studentsSchema = new studentsModel(body);
        await studentsSchema.save();
        return studentsSchema;
    } catch (error) {
        res.status(error.status).json({error: error.message});
        
    }
}

const update = async (id, body, res) => {

    try {
        await studentsModel.findByIdAndUpdate(id, body);
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
        
    }
}

const destroy = async (id, res) => {

    try {
        await studentsModel.findByIdAndDelete(id);
        return true;
    } catch (error) {
        res.status(error.status).json({error: error.message});
        
    }
}

module.exports = {index, find, store, update, destroy}